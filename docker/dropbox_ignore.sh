#/bin/bash
# https://help.dropbox.com/ja-jp/files-folders/restore-delete/ignored-files
if [ "$(uname)" == 'Darwin' ]; then
    cd "$(dirname "$0")"
fi
# ここに除外するファイルを入力する。
ignore_name=("node_modules" ".idea" "db")
for file in ${ignore_name[@]}
do
    if [ -e $file ];then
        if [ "$(uname)" == 'Darwin' ]; then
            # Mac
            xattr -w com.dropbox.ignored 1 "${PWD}/${file}"
            echo "--- ${PWD}/${file}  is dropbox ignores ---"
        elif [ "$(expr substr $(uname -s) 1 5)" == 'Linux' ]; then
            # Linux (未検証)
            attr -s com.dropbox.ignored -V 1 "${PWD}/${file}"
            echo "--- ${PWD}/${file}  is dropbox ignores ---"
        fi
    fi
done
exit 0
