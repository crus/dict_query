# -*- coding: utf-8 -*-
# v0.1
from enum import Enum
from typing import List, Dict, Union, Tuple, Callable
from datetime import datetime
from uuid import uuid4
import copy
import re

# 何かしらの値が入っているタイプ
UnionValue = Union[str, int, float, datetime]
# UnionValueにNoneも許可するタイプ
UnionValueNone = Union[str, int, float, datetime, None]

SERIAL_ID_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT"


class Settings(object):
    def __init__(self):
        self.table_quote: str = ""
        self.column_quote: str = ""
        self.value_str_quote: str = "'"
        self.terminate: str = ";"
        self.is_column_join_table_name: bool = False
        self.format_value_int: str = "{0}"
        self.format_value_float: str = "{:.10}"
        self.format_value_str: str = r"{1}{0}{1}"
        self.format_value_datetime: str = r"%Y/%m/%d %H:%M:%S"


class QueryType(Enum):
    Insert = 0
    Update = 1
    Select = 2
    Delete = 3


class QueryMaker(object):
    class FormatColumn(object):
        def __init__(self,
                     insert: Union[Callable[[UnionValueNone], str], None] = None,
                     update: Union[Callable[[UnionValueNone], str], None] = None,
                     select_after: Union[Callable[[UnionValueNone], str], None] = None,
                     is_use: bool = True,
                     is_empty_use: bool = True,
                     column_type: str = "TEXT"
                     ) -> None:
            # insertの際に使うメソッド
            self.insert_function: Union[Callable[[UnionValueNone], str], None] = insert
            # updateの際に使うメソッド
            self.update_function: Union[Callable[[UnionValueNone], str], None] = update
            # selectの際に得られた結果を所定の内容に変換するメソッド
            self.select_after_function: Union[Callable[[UnionValueNone], str], None] = select_after
            # 使用するかどうか
            self.is_use: bool = is_use
            # 指定しなくても使用するかどうか。
            self.is_empty_use: bool = is_empty_use
            # カラムの種類
            self.column_type: str = column_type

        def as_empty_use(self, value: bool) -> None:
            """
            updateやinsertの引数に指定しなくても空を引数に入れてメソッドを使用するかどうか。
            updatedやcreatedのメソッドの時とかに使用。
            :param value: 使用する
            """
            self.is_empty_use = value

        def as_use(self, value: bool) -> None:
            """
            メソッドを使用するかどうか。
            :param value: 使用する
            """
            self.is_use = value

        def set_functions(self,
                          insert: Union[Callable[[UnionValueNone], str], None],
                          update: Union[Callable[[UnionValueNone], str], None],
                          select_after: Union[Callable[[UnionValueNone], str], None]
                          ) -> None:
            self.insert_function = insert
            self.update_function = update
            self.select_after_function = select_after

    def __init__(self, table: str) -> None:
        """
        Queryを作成するためのクラス
        :param table: テーブル名
        """
        self.__table = table
        self.__format_columns: Dict[str, QueryMaker.FormatColumn] = {}
        self.__settings = Settings()
        # 比較演算子の判定オブジェクト
        self.search_operator1 = re.compile('[<>=]+')

    def set_settings(self, settings: Settings):
        self.__settings = settings

    #
    # 特殊なカラム用処理
    #
    def add_format_column(self, column_name: str,
                          insert: Callable[[UnionValueNone], str] = None,
                          update: Callable[[UnionValueNone], str] = None,
                          select_after: Callable[[UnionValueNone], str] = None,
                          is_use: bool = True,
                          is_empty_use: bool = True,
                          column_type: str = "TEXT"
                          ) -> None:
        """
        特殊なカラムを追加
        :param column_type:
        :param column_name:
        :param insert:
        :param update:
        :param select_after:
        :param is_use:
        :param is_empty_use:
        """
        self.__format_columns[column_name] = self.FormatColumn(insert=insert, update=update,
                                                               select_after=select_after,
                                                               is_use=is_use, is_empty_use=is_empty_use,
                                                               column_type=column_type)

    def add_key_column(self, column_name: str = "uid", column_type: str = "TEXT") -> None:
        """
        クエリ作成時に作成日を追加する。
        :param column_name:
        :param column_type:
        """
        self.add_format_column(column_name=column_name,
                               insert=lambda arg: str(uuid4()),
                               update=None,
                               select_after=None,
                               is_use=True,
                               is_empty_use=True,
                               column_type=column_type
                               )

    def add_serial_column(self, column_name: str = "__id",
                          column_type: str = SERIAL_ID_TYPE) -> None:
        """
        シリアル番号を追加する。
        :param column_name:
        :param column_type:
        """
        self.add_format_column(column_name=column_name,
                               insert=None,
                               update=None,
                               select_after=None,
                               is_use=True,
                               is_empty_use=False,
                               column_type=column_type
                               )

    def add_created_column(self, column_name: str = "created_at", column_type: str = "TEXT") -> None:
        """
        クエリ作成時に作成日を追加する。
        :param column_type:
        :param column_name:
        """
        self.add_format_column(column_name=column_name,
                               insert=lambda arg: datetime.now().strftime(self.__settings.format_value_datetime),
                               update=None,
                               select_after=None,
                               is_use=True,
                               is_empty_use=True,
                               column_type=column_type
                               )

    def add_updated_column(self, column_name: str = "updated_at", column_type: str = "TEXT") -> None:
        """
        クエリ作成時に修正日を追加する。
        :param column_type:
        :param column_name:
        """
        self.add_format_column(column_name=column_name,
                               insert=lambda arg: datetime.now().strftime(self.__settings.format_value_datetime),
                               update=lambda arg: datetime.now().strftime(self.__settings.format_value_datetime),
                               select_after=None,
                               is_use=True,
                               is_empty_use=True,
                               column_type=column_type
                               )

    def add_format_data(self, data: Dict[str, UnionValueNone]) \
            -> Dict[str, UnionValueNone]:
        """
        追加のcolumnのデータをつなげて返す。
        convert_dataと組み合わせて使うことが多い
        self.convert_data(data=self.marge_add_column(data),query_type=TableQuery.QueryType.Insert)
        :param data: query data
        :return: query data
        """
        result: Dict[str, UnionValueNone] = data.copy()
        keys = list(data.keys())
        for k, column in self.__format_columns.items():
            if column.is_empty_use:
                if k not in keys:
                    result[k] = None
        return result

    def convert_data(self,
                     data: Dict[str, UnionValueNone],
                     query_type: QueryType) -> Dict[str, UnionValue]:
        """
        クエリ用にデータを変換する。
        返り値にNoneは非推奨。
        marge_add_columnと組み合わせて使うことが多い。
        :param data: 元データ
        :param query_type: 対象のクエリ
        :return: query data
        """
        result: Dict[str, UnionValue] = {}
        for k, v in data.items():
            once = v
            for k2, column in self.__format_columns.items():
                if column.is_use and k == k2:
                    if query_type == QueryType.Insert:
                        if column.insert_function is not None:
                            once = column.insert_function(once)
                    elif query_type == QueryType.Update:
                        if column.update_function is not None:
                            once = column.update_function(once)
                    elif query_type == QueryType.Select:
                        if column.select_after_function is not None:
                            once = column.select_after_function(once)
            if once is not None:
                result[k] = once
        return result

    def keys_to_str(self, keys: Union[Dict[str, any],
                                      Dict[str, Union[UnionValue, List]],
                                      Dict[str, Dict[str, any]]],
                    settings: Settings, attach: str = "AND") -> str:
        """
        :examples:
query1 = mk.convert_keys_query(keys={"a": "a1", "b": "b1"}, settings=settings)
# (a='a1' AND b='b1')
query2 = mk.convert_keys_query(keys={"or": {"c1": "a", "c2": 2}}, settings=settings)
# ((c1='a' OR c2=2))
query3 = mk.convert_keys_query(keys={"and,or": {"a": "b", "c": "d", "e": "f"}}, settings=settings)
# ((a='b' AND c='d' OR e='f'))
        :param attach:
        :param keys:
        :param settings:
        :return:
        """
        result = ""
        key_list = attach.split(",")
        keys_len = len(list(keys.keys()))
        if len(key_list) == 1 and keys_len != 1:
            # ANDやORが１つだけの場合、keysの個数-1小作成する。
            key_list = key_list * (keys_len - 1)
        if keys_len - 1 != len(key_list) and keys_len != 1:
            # 個数に矛盾が生じた
            raise KeyError("'AND OR' wards count != 'A=B' wards count -1.")
        i = -1  # key_listのイテレータ
        for k, v in keys.items():
            if result != "":
                result = result + " {} ".format(key_list[i].upper())
            if isinstance(v, dict):
                result = result + self.keys_to_str(keys=v, settings=settings, attach=k)
            else:
                if isinstance(v, list):
                    result2 = ""
                    k2 = k.upper()
                    for items in v:
                        if result2 != "":
                            result2 = result2 + " {} ".format(k2)
                        result2 = result2 + self.keys_to_str(keys=items,
                                                             settings=settings, attach=k2)
                    result = result + "({})".format(result2)
                else:
                    # キーに比較演算子が合った場合は、それを使用する
                    s = self.search_operator1.search(k)
                    if s is not None:
                        search = s.span()
                        result = result + "{0}{1}{2}".format(k[:search[0]], k[search[0]:search[1]],
                                                             self.format_value_str(v, settings))
                    else:
                        # result = result + "{0}={1}".format(k, self.format_value_str(v, settings))
                        result = result + "{0}={1}".format(self.format_column_name(k, settings=settings),
                                                           self.format_value_str(v, settings))
            i = i + 1
        return "({})".format(result)

    #
    # 各種フォーマット
    #
    def format_column_name(self, name: str, settings: Settings) -> str:
        """
        カラム名に装飾を加えたものを返す
        :param settings:
        :param name: カラム名
        :return: カラム名に装飾を加えたもの
        """
        table_name = "{0}.".format(
            self.get_format_table_name(settings=settings)) if settings.is_column_join_table_name else ""
        return "{2}{1}{0}{1}".format(name, settings.column_quote, table_name)

    def get_format_table_name(self, settings: Settings) -> str:
        """
        テーブル名に装飾を加えたものを返す
        :param settings:
        :return: テーブル名に装飾を加えたもの
        """
        return "{1}{0}{1}".format(self.__table, settings.table_quote)

    def get_table(self) -> str:
        """
        設定しているテーブル名を返す
        :return: テーブル名
        """
        return self.__table

    def format_columns_str(self, columns: Union[List[str], Tuple[str, ...]], settings: Settings) -> str:
        """
        カラム名のクエ利用文字列を返す
        :param settings:
        :rtype: str
        :param columns: カラムリスト
        :return: カラム名
        """
        result = ""
        for column in columns:
            if result != "":
                result = result + ","
            result = "{} {}".format(result, self.format_column_name(column, settings))
        return result

    def format_value_str(self, value: Union[int, str, float, datetime], settings: Settings) -> str:
        """

        :param settings:
        :param value:
        :return:
        """
        if isinstance(value, int):
            return settings.format_value_int.format(str(value))
        elif isinstance(value, float):
            return settings.format_value_float.format(value)
        elif isinstance(value, str):
            return settings.format_value_str.format(value, settings.value_str_quote)
        elif isinstance(value, datetime):
            return value.strftime(self.__settings.format_value_datetime)
        else:
            raise ValueError("Unknown Value Type")
        pass

    #
    # アクセサ
    #
    def set_table(self, table: str) -> None:
        """
        テーブル名を設定
        :param table: テーブル名
        """
        self.__settings.table = table

    def set_table_quote(self, quote: str) -> None:
        """
        テーブル名の前後につけるクォーテーションを設定
        :param quote:
        """
        self.__settings.table_quote = quote

    def set_column_quote(self, quote: str) -> None:
        """
        テーブル名の前後につけるクォーテーションを設定
        :param quote:
        """
        self.__settings.column_quote = quote

    def set_terminate(self, terminate: str) -> None:
        """
        クエリの末尾につける文字
        :param terminate: 文字
        """
        self.__settings.terminate = terminate

    def as_column_join_table_name(self, value: bool) -> None:
        """
        カラム名にテーブル名を装飾するかどうか
        :param value:
        """
        self.__settings.is_column_join_table_name = value

    def divide_key_value(self, data: Dict[str, UnionValue], settings: Settings) -> Tuple[str, str]:
        """
        dictからkeyとvalueに分けてタプルで返す。
        :param data:
        :param settings:
        :return:
        """
        keys = ""
        values = ""
        for k, v in data.items():
            if keys != "":
                keys = keys + ","
            if values != "":
                values = values + ","
            keys = keys + self.format_column_name(k, settings)
            values = values + self.format_value_str(v, settings)
        return keys, values

    def join_key_value(self, data: Dict[str, UnionValue], settings: Settings) -> str:
        """
        dataのkeyとvalueで結合した状態のクエリを返す
        :param data:
        :param settings:
        :return:
        """
        result = ""
        for k, v in data.items():
            if result != "":
                result = result + ","
            result = "{}={}".format(
                result + self.format_column_name(k, settings),
                self.format_value_str(v, settings)
            )
        return result

    def format_dict_create_parts(self, data: Dict[str, UnionValue]) -> str:
        result = ""
        tmp_data = data.copy()
        # TableQuery.AddColumn
        for k, v in self.__format_columns.items():
            if v.is_use:
                tmp_data[k] = v.column_type
        for k, v in tmp_data.items():
            if result != "":
                result = result + ","
            result = "{} {}".format(
                result + k,
                v
            )
        return result

    def format_where(self, keys: Union[Dict[str, UnionValue], None],
                     where: Union[str, None] = None, settings: Settings = None, attach: str = "AND") -> str:
        tmp_where = ""
        if keys is not None:
            tmp_where = self.keys_to_str(keys=keys, settings=settings)
        if where:
            if tmp_where != "":
                tmp_where = tmp_where + " {0} ".format(attach)
            tmp_where = "{0}{1}".format(tmp_where, where)
        return tmp_where

    #
    # CRUD
    #
    def insert(self, data: Union[Dict[str, UnionValue],
                                 List[Dict[str, UnionValue]]],
               settings: Union[Settings, None] = None,
               left: Union[str, None] = None, right: Union[str, None] = None,
               ) -> Dict[str, Union[List[str], str, Dict[str, UnionValue], List[Dict[str, UnionValue]]]]:
        """
        insert
        :param data:
        :param left:
        :param right:
        :param settings:
        :return:
        """
        # セッティング
        tmp_settings = copy.deepcopy(self.__settings) if settings is None else settings
        tmp_right = right if right is not None else ""
        tmp_left = left if left is not None else ""
        # noinspection SqlNoDataSourceInspection
        format_str = r"{0}INSERT INTO {1} ({2})VALUES({3}){4}{5}"
        if isinstance(data, dict):
            query_data = self.convert_data(data=self.add_format_data(data),
                                           query_type=QueryType.Insert)
            key, value = self.divide_key_value(data=query_data, settings=tmp_settings)
            return {"query": format_str.format(
                tmp_left,
                self.get_format_table_name(settings=tmp_settings),
                key,
                value,
                tmp_right,
                tmp_settings.terminate),
                "data": query_data}
        elif isinstance(data, list):
            result = []
            result_query_data = []
            for item in data:
                query_data = self.convert_data(data=self.add_format_data(item),
                                               query_type=QueryType.Insert)
                result_query_data.append(query_data)
                key, value = self.divide_key_value(data=query_data, settings=tmp_settings)
                result.append(
                    format_str.format(self.get_format_table_name(settings=tmp_settings), key, value,
                                      tmp_settings.terminate))
            return {"query": result, "data": result_query_data}
        return {"data": data, "query": ""}

    def update(self, data: Dict[str, UnionValue],
               keys: Union[Dict[str, any], Dict[str, Union[UnionValue, List]], Dict[str, Dict[str, any]], None] = None,
               where: Union[str, None] = None,
               settings: Union[Settings, None] = None,
               left: Union[str, None] = None, right: Union[str, None] = None,
               ) -> Dict[str, Union[List[str], str, Dict[str, UnionValue], List[Dict[str, UnionValue]]]]:
        """
        update
        :param left:
        :param right:
        :param keys:
        :param where:
        :param data:
        :param settings:
        :return:
        """
        # セッティング
        tmp_settings = copy.deepcopy(self.__settings) if settings is None else settings
        tmp_right = right if right is not None else ""
        tmp_left = left if left is not None else ""
        # language=PostgreSQL
        # noinspection SqlNoDataSourceInspection
        format_str = "{0}UPDATE {1} SET {2} {3}{4}{5}"
        if not keys and not where:
            """updateは必ず条件式が必要です"""
            raise ValueError
        tmp_where = "WHERE " + self.format_where(keys=keys, where=where, settings=tmp_settings)
        if isinstance(data, dict):
            query_data = self.convert_data(data=self.add_format_data(data),
                                           query_type=QueryType.Update)
            value = self.join_key_value(data=query_data, settings=tmp_settings)
            return {"query": format_str.format(
                tmp_left,
                self.get_format_table_name(settings=tmp_settings),
                value,
                tmp_where,
                tmp_right,
                tmp_settings.terminate),
                "data": query_data}
        elif isinstance(data, list):
            result = []
            result_query_data = []
            for item in data:
                query_data = self.convert_data(data=self.add_format_data(item),
                                               query_type=QueryType.Update)
                value = self.join_key_value(data=query_data, settings=tmp_settings)
                result_query_data.append(query_data)
                result.append(
                    format_str.format(
                        tmp_left,
                        self.get_format_table_name(settings=tmp_settings),
                        value,
                        tmp_where,
                        tmp_right,
                        tmp_settings.terminate))
            return {"query": result, "data": result_query_data}
        return {"data": data, "query": ""}

    def select(self,
               columns: Union[List[str], Tuple[str, ...], None],
               keys: Union[Dict[str, any], Dict[str, Union[UnionValue, List]], Dict[str, Dict[str, any]], None] = None,
               left: Union[str, None] = None, right: Union[str, None] = None, where: Union[str, None] = None,
               settings: Union[Settings, None] = None
               ) -> str:
        """
        SELECT文を返す
        :param where:
        :param keys:
        :param columns: カラムリスト
        :param left:
        :param right: 条件式
        :param settings:
        :return: query
        """
        # セッティング
        tmp_settings = copy.deepcopy(self.__settings) if settings is None else settings
        # keys
        tmp_keys = " WHERE " + self.format_where(keys=keys, where=where,
                                                 settings=tmp_settings) if keys is not None else ""
        tmp_right = right if right is not None else ""
        tmp_left = left if left is not None else ""
        column_str: str = r" *" if columns is None else self.format_columns_str(columns=columns, settings=tmp_settings)
        # language=PostgreSQL
        # noinspection SqlNoDataSourceInspection,SqlDialectInspectionForFile
        sql = """{0}SELECT{1} FROM {2}{3}{4}""".format(
            tmp_left,
            column_str,
            self.get_format_table_name(settings=tmp_settings),
            tmp_keys,
            tmp_right,
            tmp_settings.terminate,
        )
        return sql

    def delete(self,
               keys: Union[Dict[str, any], Dict[str, Union[UnionValue, List]], Dict[str, Dict[str, any]], None] = None,
               where: Union[str, None] = None,
               settings: Union[Settings, None] = None,
               left: Union[str, None] = None, right: Union[str, None] = None,
               ) -> str:
        """
        delete
        :param settings:
        :param keys:
        :param where:
        :param left:
        :param right:
        :return:
        """
        # セッティング
        tmp_settings = copy.deepcopy(self.__settings) if settings is None else settings
        tmp_right = right if right is not None else ""
        tmp_left = left if left is not None else ""
        # language=PostgreSQL
        # noinspection SqlNoDataSourceInspection
        format_str = """{0}DELETE FROM {1} {2}{3}{4}"""
        if not keys and not where:
            """deleteは必ず条件式が必要です"""
            raise ValueError
        after_str = right if right is not None else ""
        return format_str.format(
            tmp_left,
            self.get_format_table_name(settings=tmp_settings),
            "WHERE " + self.format_where(keys=keys, where=where, settings=tmp_settings),
            after_str,
            tmp_right,
            tmp_settings.terminate)

    def create(self, key_type: Dict[str, str],
               settings: Union[Settings, None] = None) -> str:
        # セッティング
        tmp_settings = copy.deepcopy(self.__settings) if settings is None else settings
        # PostgreSQL
        # noinspection SqlNoDataSourceInspection
        format_str = """CREATE TABLE IF NOT EXISTS {0} ({1}){2}"""
        return format_str.format(self.get_table(), self.format_dict_create_parts(key_type), tmp_settings.terminate)
