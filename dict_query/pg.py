# -*- coding: utf-8 -*-
# v0.2
import psycopg2
from psycopg2._psycopg import connection as Connection
from psycopg2._psycopg import cursor as Cursor
import configparser
from .base import QueryMaker, UnionValue, UnionValueNone, QueryType
from typing import List, Dict, Union, Tuple

SERIAL_ID_TYPE = "SERIAL NOT NULL"


class Postgres(object):
    def __init__(self, host: str, database: str, user: str, password: str, table_name: str,
                 port: Union[str, int] = 5432):
        # 対象のファイル名
        self.__host: str = host
        self.__port: str = ""
        self.__database: str = database
        self.__user: str = user
        self.__password: str = password
        self.__table: str = table_name
        self.__table_query: QueryMaker = QueryMaker(table_name)
        self.__is_created_db = True
        self.set_port(port)
        return

    def basic_init(self, column_key_type: Dict[str, str]):
        """
        標準的な初期化
        :return:
        """
        self.add_serial_column(column_name="__id", )
        self.add_key_column(column_name="uid", column_type="TEXT NOT NULL")
        self.add_updated_column("updated_at", column_type="TIMESTAMP NOT NULL DEFAULT current_timestamp")
        self.add_created_column("created_at", column_type="TIMESTAMP NOT NULL DEFAULT current_timestamp")
        # http://to-c.hatenablog.com/entry/2017/04/17/225826
        # 初回でテーブルを作成した、かつ、テーブルが存在している状態
        self.create_table(column_key_type=column_key_type)
        if not self.__is_created_db and self.is_table():
            try:
                # language=PostgreSQL
                self.execute(r"""-- noinspection SqlNoDataSourceInspectionForFile
create function set_update_time() returns opaque as '
  begin
    new.updated_at := ''now'';
    return new;
  end;
' language 'plpgsql';""")
            except BaseException as e:
                print(e)
            try:
                # language=PostgreSQL
                # noinspection SqlNoDataSourceInspection,SqlDialectInspection
                self.execute(r"""create trigger update_tri before update on {0} for each row
  execute procedure set_update_time();""".format(self.__table))
            except BaseException as e:
                print(e)

    def set_port(self, value: Union[int, str]) -> None:
        self.__port = str(value)

    @staticmethod
    def __execute_once(cursor: psycopg2, query: str) -> None:
        try:
            cursor.execute(query)
        except (psycopg2.OperationalError, psycopg2.ProgrammingError) as e:
            cursor.close()
            print(query)
            print(e)
            # raise psycopg2.OperationalError("Query Error!")
        return None

    def execute(self, queries: Union[List[str], Tuple[str, ...], str],
                is_query_marge=True,
                connection: Union[Connection, None] = None
                ) -> Union[List[Tuple[UnionValue, ...]], None]:
        def get_rows(cursor2: Cursor) -> List[Tuple[UnionValueNone, ...]]:
            result2 = []
            if cursor2.rowcount > 0:
                # 結果がSELECTの場合
                if "select" in cursor2.statusmessage.lower():
                    for row2 in cursor2:
                        result2.append(row2)
            return result2

        if len(queries) == 0:
            return None
        query_str = ""
        result = []
        if is_query_marge:
            if isinstance(queries, list) or isinstance(queries, tuple):
                for q in queries:
                    query_str = query_str + q
            elif isinstance(queries, str):
                query_str = queries
            if not isinstance(query_str, str):
                raise TypeError("queries is do not str list or str tuple or str!")
            elif isinstance(query_str, str):
                con = self.connection() if connection is None else connection
                cursor = con.cursor()
                self.__execute_once(cursor, query_str)
                result = get_rows(cursor2=cursor)
                if connection is None:
                    # connectionを指定していない場合はこの時点でコミットする。
                    # 複数の内容があるのであれば、connectionは指定したほうが良い
                    """EXAMPLE
with pg.connection() as con:
    obj2, query2 = pg.insert(data={"c1": "a", "c2": 1, "C3": 0.1},connection=con)
    print(obj2, query2)
    print(pg.update(data={"c1": "b", "c2": 2, "C3": 0.2}, keys={"uid": obj2["uid"]}, connection=con))
    print(pg.select(["uid"], "WHERE c2=2", connection=con))
                    """
                    con.commit()
                    cursor.close()
                    con.close()
            else:
                raise TypeError("queries is do not list or tuple or str!")
        elif isinstance(queries, tuple) or isinstance(queries, list):
            for query in queries:
                con = self.connection() if connection is None else connection
                with con.cursor() as cursor:
                    self.__execute_once(cursor, query)
                    result = get_rows(cursor2=cursor)
                if connection is None:
                    con.close()
        else:
            raise TypeError("queries is do not list or tuple or str!")
        return result

    def connection(self) -> Connection:
        try:
            return psycopg2.connect("host={0} port={1} dbname={2} user={3} password={4}".format(
                self.__host,
                self.__port,
                self.__database,
                self.__user,
                self.__password
            ))
        except psycopg2.OperationalError:
            raise psycopg2.OperationalError("Connect Found!")

    def create_table(self, column_key_type: Dict[str, str]) -> None:
        """
        テーブル作成。
        :param column_key_type: {"c1": "TEXT", "C2", "INTEGER"}
        """
        if not self.is_table():
            self.execute(queries=self.__table_query.create(column_key_type),
                         is_query_marge=True, connection=None)
            self.__is_created_db = False

    def is_table(self):
        result = False
        with self.connection() as con:
            with con.cursor() as cursor:
                try:
                    # language=PostgreSQL
                    # noinspection SqlNoDataSourceInspection,SqlDialectInspection
                    q = r"""SELECT * FROM information_schema.tables WHERE table_name='{0}';""".format(
                        self.__table
                    )
                    cursor.execute(q)
                    result = bool(cursor.rowcount)
                except psycopg2.OperationalError:
                    cursor.close()
                    con.close()
                    raise psycopg2.OperationalError("Connect Found!")
        return result

    def add_serial_column(self, column_name: str = "__id", column_type: str = SERIAL_ID_TYPE) -> None:
        """
        クエリ作成時に作成日を追加する。
        :param column_type:
        :param column_name:
        """
        self.__table_query.add_serial_column(column_name=column_name, column_type=column_type)

    def add_key_column(self, column_name: str = "uid", column_type: str = "TEXT") -> None:
        """
        クエリ作成時に作成日を追加する。
        :param column_type:
        :param column_name:
        """
        self.__table_query.add_key_column(column_name=column_name, column_type=column_type)

    def add_created_column(self, column_name: str = "created_at", column_type: str = "TIMESTAMP") -> None:
        """
        クエリ作成時に作成日を追加する。
        :param column_type:
        :param column_name:
        """
        self.__table_query.add_created_column(column_name=column_name, column_type=column_type)

    def add_updated_column(self, column_name: str = "updated_at", column_type: str = "TEXT") -> None:
        """
        クエリ作成時に修正日を追加する。
        :param column_type:
        :param column_name:
        """
        self.__table_query.add_updated_column(column_name=column_name, column_type=column_type)

    def add_format_column(self, column_name: str, insert=None, update=None,
                          select_after=None,
                          is_use=True, is_empty_use=True,
                          column_type="TEXT"):
        self.__table_query.add_format_column(column_name=column_name, insert=insert, update=update,
                                             select_after=select_after,
                                             is_use=is_use, is_empty_use=is_empty_use,
                                             column_type=column_type)

    def get_table_name(self):
        return self.__table_query.get_table()

    #
    # CRUD
    #
    def insert(self, data: Union[Dict[str, UnionValue], List[Dict[str, UnionValue]]],
               connection: Union[Connection, None] = None,
               left: Union[str, None] = None, right: Union[str, None] = None,
               ) -> Dict[str, Union[Dict[str, UnionValue], str]]:
        query = self.__table_query.insert(data=data, left=left, right=right)
        self.execute(queries=query["query"], is_query_marge=True, connection=connection)
        return {"query": query["query"], "data": query["data"]}

    def update(self, data: Union[Dict[str, UnionValue], List[Dict[str, UnionValue]]],
               keys: Union[Dict[str, UnionValue], None],
               where: Union[str, None] = None,
               connection: Union[Connection, None] = None,
               left: Union[str, None] = None, right: Union[str, None] = None,
               ) -> Dict[str, Union[Dict[str, UnionValue], str]]:
        query = self.__table_query.update(data=data, keys=keys, where=where, settings=None,
                                          left=left, right=right)
        self.execute(queries=query["query"], is_query_marge=True, connection=connection)
        return {"query": query["query"], "data": query["data"]}

    def select(self, columns: Union[List[str], Tuple[str, ...], None],
               keys: Union[Dict[str, any], Dict[str, Union[UnionValue, List]], Dict[str, Dict[str, any]], None] = None,
               connection: Union[Connection, None] = None,
               left: Union[str, None] = None, right: Union[str, None] = None, where: Union[str, None] = None
               ) -> Union[List[Dict[str, UnionValueNone]], List[Tuple[UnionValueNone]], Tuple[None], None]:
        query = self.__table_query.select(columns=columns, keys=keys, left=left, right=right, where=where)
        result = self.execute(queries=query, is_query_marge=True, connection=connection)
        # 検索キーが*の場合
        if not isinstance(result, list):
            return result
        if result is None:
            return None
        if len(result) == 0:
            return result
        if len(result[0]) != len(columns):
            return result
        obj = []
        for values in result:
            once = {}
            for key, value in zip(columns, values):
                once[key] = value
            once = self.__table_query.convert_data(data=once, query_type=QueryType.Select)
            obj.append(once)
        return obj

    def delete(self,
               keys: Union[Dict[str, UnionValue], None],
               where: Union[str, None] = None,
               left: Union[str, None] = None, right: Union[str, None] = None,
               connection: Union[Connection, None] = None
               ):
        self.execute(queries=self.__table_query.
                     delete(keys=keys, where=where,
                            left=left, right=right),
                     connection=connection)

    @staticmethod
    def commit(connection: Connection):
        connection.commit()


def ini(path: str, table: str, encoding: str = "utf-8") -> Postgres:
    c: configparser.ConfigParser = configparser.ConfigParser()
    c.read(path, encoding=encoding)
    return Postgres(host=c['root']['host'],
                    port=c['root']['port'],
                    database=c['root']['database'],
                    user=c['root']['user'],
                    password=c['root']['password'],
                    table_name=table,
                    )
