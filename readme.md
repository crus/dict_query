# Dict Query


"Dict Query" is a query generation module that uses associative arrays.

## Installation

Python >= 3.6 

## usage

Query maker.

```python
from dict_query import  QueryMaker

mk = QueryMaker("test")
# select
print(mk.select(columns=["a", "b", "c"], right="WHERE 1=1"))
print(mk.select(columns=None, right="WHERE 1=1"))
# insert
print(mk.insert(data={"a": 1, "b": "c"}))
# update
print(mk.update(data={"a": 1, "b": "c"}, keys={"d": "e"}))
print(mk.update(data={"a": 1, "b": "c"}, keys={"or": {"d<": "e", "f>=": "g"}}))
print(mk.update(data={"a": 1, "b": "c"}, keys={"or not": {"d<": "e", "f": "g"}}))
print(mk.update(data={"a": 1}, keys={"and,or": {"d": "e", "f": "g", "i": "j"}}))
print(mk.update(data={"a": 1, "b": "c"}, where="h=1"))
print(mk.update(data={"a": 1, "b": "c"}, keys={"d": "e"}, where="h=1"))
# delete
print(mk.delete(keys={"d": "e"}))
print(mk.delete(keys={"or": {"d<": "e", "f>=": "g"}}))
print(mk.delete(where="h=1"))
# settings object
settings = Settings()
settings.is_column_join_table_name = True
settings.table_quote = "\""
settings.column_quote = "\""
print(mk.select(columns=["a", "b"], settings=settings))

"""
SELECT a, b, c FROM test WHERE 1=1;
SELECT * FROM test WHERE 1=1;
{'query': "INSERT INTO test (a,b)VALUES(1,'c');", 'data': {'a': 1, 'b': 'c'}}
{'query': "UPDATE test SET a=1,b='c' WHERE (d='e');", 'data': {'a': 1, 'b': 'c'}}
{'query': "UPDATE test SET a=1,b='c' WHERE ((d<'e' OR f>='g'));", 'data': {'a': 1, 'b': 'c'}}
{'query': "UPDATE test SET a=1,b='c' WHERE ((d<'e' OR NOT f='g'));", 'data': {'a': 1, 'b': 'c'}}
{'query': "UPDATE test SET a=1 WHERE ((d='e' AND f='g' OR i='j'));", 'data': {'a': 1}}
{'query': "UPDATE test SET a=1,b='c' WHERE h=1;", 'data': {'a': 1, 'b': 'c'}}
{'query': "UPDATE test SET a=1,b='c' WHERE (d='e') AND h=1;", 'data': {'a': 1, 'b': 'c'}}
DELETE FROM test WHERE (d='e');
DELETE FROM test WHERE ((d<'e' OR f>='g'));
DELETE FROM test WHERE h=1;
SELECT "test"."a", "test"."b" FROM "test";
"""
```

## PostgreSQL access.

How to connect PostgreSQL.
Based on the `dict_query/pg.py` file, customize it according to the database you want to use.

### install PostgreSQL module

Required external `psycopg2` packages.

#### for mac

```bash
brew install postgresql
brew install openssl
pip install psycopg2
```

### usage

```python
from dict_query import pg_ini
# read setting file. and table name
pg = pg_ini("./pg_connect.ini", "test")
# (OPTIONAL) initialize. 
# Do some basic initialization.
# Will be added automatically column.... uid (UUID) and created_at(add create timestamp) and updated_at (add updated timestamp)
pg.basic_init(column_key_type={"c1": "TEXT", "c2": "INTEGER", "C3": "REAL"})
# insert
obj = pg.insert(data={"c1": "a", "c2": 1, "C3": 0.1})
print(obj["data"], obj["query"])
obj2 = pg.update(data={"c1": "b", "c2": 2, "C3": 0.2}, keys={"uid": obj["data"]["uid"]})
print(pg.select(columns=["uid"],after= "WHERE c2=2"))
# By preparing a connection, processing can be performed at higher speed.
with pg.connection() as con:
    obj3 = pg.insert(data={"c1": "a", "c2": 1, "C3": 0.1}, connection=con)
    print(obj3["query"])
    print(obj3["data"])
    print(pg.update(data={"c1": "b", "c2": 2, "C3": 0.2}, keys={"uid": obj3["data"]["uid"]}, connection=con))
```

## License
[MIT](https://choosealicense.com/licenses/mit/)