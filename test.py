#! env python
# -*- coding: utf-8 -*-

import os
import sys
import re
import binascii

import unittest
from dict_query import pg_ini, QueryMaker, jdbc_ini, Settings, Sqlite

# Google python Style Guide
# https://google.github.io/styleguide/pyguide.html
# http://works.surgo.jp/translation/pyguide.html

__pychecker__ = 'no-callinit no-classattr'

__author__ = 'MM'
__date__ = "2020/10/03"


def test_query():
    os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))
    db = QueryMaker("test")
    assert db.create(key_type={"c1": "INTEGER", "c2": "TEXT",
                               "c3": "REAL"}) == r"CREATE TABLE IF NOT EXISTS test (c1 INTEGER,c2 TEXT,c3 REAL);"
    assert db.insert(data={"c1": 1, "c2": "test", "c3": 0.001}) == {
        "query": "INSERT INTO test (c1,c2,c3)VALUES(1,'test',0.001);",
        "data": {'c1': 1, 'c2': 'test', 'c3': 0.001}}
    assert db.update(data={"c2": "test2"}, keys={"c1": 1}) == {'query': "UPDATE test SET c2='test2' WHERE (c1=1);",
                                                               'data': {'c2': 'test2'}}
    assert db.select(columns=("c1", "c2",), right="WHERE 1=1") == "SELECT c1, c2 FROM test WHERE 1=1;"
    assert db.select(columns=None) == "SELECT * FROM test;"
    assert db.delete(keys={"c1": 1}) == "DELETE FROM test WHERE (c1=1);"


def example_pg():
    pg = pg_ini("./jdbc_connect.ini", "test")
    pg.basic_init(column_key_type={"c1": "TEXT", "c2": "INTEGER", "C3": "REAL"})
    obj = pg.insert(data={"c1": "a", "c2": 1, "C3": 0.1})
    print(obj["data"], obj["query"])
    obj2 = pg.update(data={"c1": "b", "c2": 2, "C3": 0.2}, keys={"uid": obj["data"]["uid"]})
    print(obj2["query"])
    print(obj2["data"])
    print(pg.select(["uid"], "WHERE c2=2"))
    print(pg.delete(keys={"uid": obj["data"]["uid"]}))
    with pg.connection() as con:
        obj3 = pg.insert(data={"c1": "a", "c2": 1, "C3": 0.1}, connection=con)
        print(obj3["query"])
        print(obj3["data"])
        print(pg.update(data={"c1": "b", "c2": 2, "C3": 0.2}, keys={"uid": obj3["data"]["uid"]}, connection=con))
        print(pg.select(["uid", "c1", "c2"], "WHERE c2=2", connection=con))
        # print(pg.delete(keys={"uid": obj3["data"]["uid"]}, connection=con))


def test_static_method():
    mk = QueryMaker("test")
    # select
    print(mk.select(columns=["a", "b", "c"], right="WHERE 1=1"))
    print(mk.select(columns=None, right="WHERE 1=1"))
    # insert
    print(mk.insert(data={"a": 1, "b": "c"}))
    # update
    print(mk.update(data={"a": 1, "b": "c"}, keys={"d": "e"}))
    print(mk.update(data={"a": 1, "b": "c"}, keys={"or": {"d<": "e", "f>=": "g"}}))
    print(mk.update(data={"a": 1, "b": "c"}, keys={"or not": {"d<": "e", "f": "g"}}))
    print(mk.update(data={"a": 1}, keys={"and,or": {"d": "e", "f": "g", "i": "j"}}))
    print(mk.update(data={"a": 1}, keys={"and": {"or": {"d": "e", "f": "g"}, "i": "j"}}))
    print(mk.update(data={"a": 1, "b": "c"}, where="h=1"))
    print(mk.update(data={"a": 1, "b": "c"}, keys={"d": "e"}, where="h=1"))
    # delete
    print(mk.delete(keys={"d": "e"}))
    print(mk.delete(keys={"or": {"d<": "e", "f>=": "g"}}))
    print(mk.delete(where="h=1"))
    # settings object
    settings = Settings()
    settings.is_column_join_table_name = True
    settings.table_quote = "\""
    settings.column_quote = "\""
    print(mk.select(columns=["a", "b"], settings=settings))
    print(mk.select(columns=["a", "b"], keys={"c": "d"}, settings=settings))


def example_sqlite():
    pg = Sqlite(file_path="example.db", table_name="examples")
    pg.basic_init(column_key_type={"c1": "TEXT", "c2": "INTEGER", "C3": "REAL"})
    obj = pg.insert(data={"c1": "a", "c2": 1, "C3": 0.1})
    print(obj["data"], obj["query"])
    obj2 = pg.update(data={"c1": "b", "c2": 2, "C3": 0.2}, keys={"uid": obj["data"]["uid"]})
    print(obj2["query"])
    print(obj2["data"])
    print(pg.select(["uid"], keys={"c2": 2}))
    print(pg.delete(keys={"uid": obj["data"]["uid"]}))
    with pg.connection() as con:
        obj3 = pg.insert(data={"c1": "a", "c2": 1, "C3": 0.1}, connection=con)
        print(obj3["query"])
        print(obj3["data"])
        print(pg.update(data={"c1": "b", "c2": 2, "C3": 0.2}, keys={"uid": obj3["data"]["uid"]}, connection=con))
        print(pg.select(["uid", "c1", "c2"], where="c2=2", connection=con))
        # print(pg.delete(keys={"uid": obj3["data"]["uid"]}, connection=con))


def main():
    # test_query()
    # example_pg()
    # test_static_method()
    # print(binascii.hexlify(b'<'))
    example_sqlite()
    return


if __name__ == '__main__':
    main()
